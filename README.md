# gen-shell
A simple way to turn any command into a REPL with arrow key/history support.

## Usage
See `gen-shell --help`

## Installation
Dependencies:  
  - POSIX make
  - a c++ compiler and standard library
  - libreadline development files (7 or 8 is fine, 5 is untested)

```bash
make
make install
```
## License
Following suit from taskshell, gen-shell is MIT licensed by Armaan Bhojwani, 2021. Gen-shell is forked from taskshell, which was developed by [these people](https://github.com/GothenburgBitFactory/taskshell/blob/master/AUTHORS).

Gen-shell uses the [Sarge library](https://github.com/MayaPosch/Sarge) for parsing command-line arguments. Sarge was written by Maya Posch and is BSD 3-Clause licensed.
